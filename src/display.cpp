#include <Arduino.h>
#include <Adafruit_SSD1331.h>
#include <Adafruit_GFX.h>

const int8_t RES = 33;
const int8_t DC = 25;
const int VCCEN = 5;
const int PMODEN = 15;

#define NOIR 0x0000
#define BLEU 0x001F
#define ROUGE 0xF800
#define BLANC 0xFFFF

Adafruit_SSD1331 afficheur = Adafruit_SSD1331(SS, DC, MOSI, SCK, RES);

void displayValue(float f){
  delay(1000);
  afficheur.fillScreen(NOIR);
  afficheur.setCursor(0,0);
  afficheur.setTextSize(3);

  afficheur.print(f);
}

void initializeLed(){
  pinMode(VCCEN, OUTPUT);
  pinMode(PMODEN, OUTPUT);
  digitalWrite(VCCEN, HIGH);
  digitalWrite(PMODEN, HIGH);

  afficheur.begin();
  afficheur.fillScreen(NOIR);
  afficheur.setTextColor(BLANC);
  afficheur.setCursor(0,0);
  afficheur.setTextSize(3);

  afficheur.print(45.51);
  delay(100);
}
