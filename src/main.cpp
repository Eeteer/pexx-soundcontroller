#include <Arduino.h>
#include <WiFi.h>
#include <PubSubClient.h>
#include "network.h"
#include "i2s.h"
#include "inmp441.h"
#include "display.h"

WiFiClient espClient;
PubSubClient client(espClient);

const char *serverMQTT = "192.168.43.84";
const int portMQTT = 1883;

char *ssidAP = "NicoTel";
char *passwordAP = "Nico_B000";

void setup() {
  setCpuFrequencyMhz(80); // It should run as low as 80MHz
  Serial.begin(112500);
  initializeLed();
  displayValue(5.55);
  delay(1000);
  displayValue(48.55);
  connectToWifi(ssidAP, passwordAP);
  client.setServer(WiFi.gatewayIP(),portMQTT);
  connectToMQTT(client);
  Serial.println("----------- After connecting -----------");
  //initialize_I2S();
  execute_inmp441(client);
}

void loop() {
  //run_I2S();
}
