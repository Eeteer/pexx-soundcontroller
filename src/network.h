#ifndef __NETWORK_H
#define __NETWORK_H

#include <PubSubClient.h>


void connectToWifi(char*,char*);
void launchSoftAP();
void connectToMQTT(PubSubClient);
void sendMQTTMessage(PubSubClient, String);
String buildMQTTMessage(int[]);


#endif
