#ifndef _INMP441_H
#define _INMP441_H

#include <PubSubClient.h>


void mic_i2s_init();
void mic_i2s_reader_task(void*);
void execute_inmp441(PubSubClient);


#endif
