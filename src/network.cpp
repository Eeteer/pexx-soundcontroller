#include <Arduino.h>
#include <WiFi.h>
#include <PubSubClient.h>
#include <iostream>
#include <sstream>
#include <string>
#include <sys/time.h>
#include "network.h"
#include "display.h"



String clientId = "ESP32CLIENT-884568";

const char *topicDb = "PEXX/db";

void connectToWifi(char *ssid, char *password){
  WiFi.begin(ssid, password);
  while(WiFi.status() != WL_CONNECTED){
    delay(500);
    Serial.println("Connecting to .....");
    Serial.println(ssid);
  }
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  displayValue(10);
}

//Show connection parameter such as @ESP-IP, @Gateway-IP and @Gatewau-mask
void showConnectionParameter(){
  Serial.println("Connected with IP :");
  IPAddress ip = WiFi.localIP();
  Serial.println(ip);
  Serial.println(" on ");
  IPAddress gw = WiFi.gatewayIP();
  Serial.println(gw);
  Serial.println(" with netmask : ");
  IPAddress masque = WiFi.subnetMask();
  Serial.println(masque);
}

void connectToMQTT(PubSubClient client) {
  // Loop until we're reconnected
  while (!client.connected()) {
    //showConnectionParameter();
    Serial.print("Attempting MQTT connection...");
    // Create a random client ID

    // Attempt to connect
    if (client.connect(clientId.c_str())) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      client.publish("outTopic", "hello world");
      // ... and resubscribe
      client.subscribe("inTopic");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }

  Serial.println("avoid mqtt connection");
}

void sendMQTTMessage(PubSubClient client, String msg){
  if(client.connect(clientId.c_str())){
    Serial.println("send message ! ");
    client.publish("pexx/esp32-1111111", msg.c_str());
    client.subscribe("pexx");
  }else{
    Serial.println("failed");
    connectToMQTT(client);
    delay(5000);
  }
}

String buildMQTTMessage(int q[]){
  int i;
  time_t     now = time(0);
  struct tm  tstruct;
  char       buf[80];
  tstruct = *localtime(&now);
  // Visit http://en.cppreference.com/w/cpp/chrono/c/strftime
  // for more information about date/time format
  strftime(buf, sizeof(buf), "%Y-%m-%d.%X", &tstruct);
  Serial.println(buf);
  std::string a = "/";
  a += buf;
  std::stringstream msgStr;
  for (i=0; i<=119;i++){
    msgStr<<"/"<<q[i];
    //Serial.println();
  }
  a += msgStr.str();
  Serial.println(a.c_str());
  return a.c_str();
}
